FROM php:7.4-apache

# Installation de Wp cli
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar

RUN chmod +x wp-cli.phar
RUN mv wp-cli.phar /usr/local/bin/wp

# Installation de composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Changement de la configuration Apache
ADD conf/vhost.conf /etc/apache2/sites-available/000-default.conf
ADD conf/apache.conf /etc/apache2/conf-available/z-app.conf
RUN a2enmod headers
RUN a2enconf z-app
RUN a2enmod rewrite

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        zlib1g-dev \
        libxml2-dev \
        libzip-dev \
        libonig-dev \
        graphviz \
    && docker-php-ext-configure gd \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install zip \
    && docker-php-ext-install exif \
    && docker-php-ext-install mysqli \
    && docker-php-source delete

ADD https://raw.githubusercontent.com/mlocati/docker-php-extension-installer/master/install-php-extensions /usr/local/bin/
RUN chmod uga+x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions xdebug imagick mcrypt calendar
#Allow the user to use wp
RUN echo 'alias wp="wp --allow-root"' >> ~/.bashrc
RUN echo 'alias test="echo hello world"' >> ~/.bashrc
#Allow Traefik to work with the container
LABEL traefik.enable=true
ADD conf/php.ini /usr/local/etc/php/conf.d/app.ini
RUN apt-get update && apt-get install curl  nano zip
CMD [ "apachectl","-D","FOREGROUND" ] 